TARGET = pulseaudio-example

TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

CONFIG += link_pkgconfig
PKGCONFIG += libpulse libpulse-mainloop-glib glib-2.0 audioresource

SOURCES += main.cpp

OTHER_FILES += rpm/*
