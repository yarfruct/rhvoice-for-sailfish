#include <iostream>

#include <glib.h>
#include <audioresource.h>

#include <pulse/pulseaudio.h>
#include <pulse/glib-mainloop.h>

using namespace std;

/*!
 * \brief Current PulseAudio loop.
 */
pa_glib_mainloop* pa_glml;

/*!
 * \brief Current PulseAudio context.
 */
pa_context* pa_ctx;

/*!
 * \brief Current PulseAudio stream.
 */
pa_stream* pastream;

/*!
 * \brief Flag to exit from the Glib event loop.
 * This value is true when the Glib loop should be stopped and false when the Glib loop is started.
 */
bool glib_loop_stopped;

/*!
 * \brief Disconnects the PulseAudio stream.
 * \param pas Current PulseAudio stream.
 * \param success True if the PulseAudio stream draining or flushing is success, false — otherwise.
 * \param userdata User data passed to the pa_stream_drain() and pa_stream_flush() methods.
 */
void disconnect_stream(pa_stream* pas, int success, void* userdata) {
    pa_stream_disconnect(pas);
}

/*!
 * \brief Disconnects and frees the PulseAudio stream, context and loop.
 */
void free_pulse_audio() {
    if (pastream) {
        pa_stream_set_write_callback(pastream, NULL, NULL);
        pa_stream_disconnect(pastream);
    }
    if (pa_ctx) {
        pa_context_set_state_callback(pa_ctx, NULL, NULL);
        pa_context_disconnect(pa_ctx);
        pa_context_unref(pa_ctx);
    }
    if (pa_glml) {
        pa_glib_mainloop_free(pa_glml);
    }
}

/*!
 * \brief Stops the given audio stream.
 * \param pas The PulseAudio stream pointer.
 * \param playing_finished True if the audio file playing is finished,
 *                          false — if the playing is stopped.
 */
void stop_audio_stream(pa_stream* pas, bool playing_finished) {
    if (pas) {
        pa_stream_cancel_write(pas);
        if (playing_finished) {
            pa_stream_drain(pas, disconnect_stream, NULL);
        } else {
            pa_stream_flush(pas, disconnect_stream, NULL);
        }
    }
}

/*!
 * \brief Plays the audio file from 'userdata' parameter using the PulseAudio Asynchronous API.
 * \param pas Current PulseAudio stream.
 * \param len Length of user data.
 * \param userdata Audio file passed during the PulseAudio stream initialization.
 */
void play_audio_stream(pa_stream* pas, size_t len, void* userdata) {
    void* buf;
    FILE* file = (FILE*) userdata;
    size_t buf_size = (size_t) -1;

    pa_stream_begin_write(pas, &buf, &buf_size);
    size_t bytes_read = fread(buf, 1, buf_size, file);
    if (bytes_read == 0) {
        stop_audio_stream(pas, true);
        glib_loop_stopped = true;
    } else {
        pa_stream_write(pas, buf, bytes_read, NULL, 0, PA_SEEK_RELATIVE);
    }
}

/*!
 * \brief Handles the PulseAudio context state changing.
 * This method frees the PulseAudio steram, context and loop when the Pulse Audio state has values
 * PA_CONTEXT_FAILED or PA_CONTEXT_TERMINATED.
 * When the PulseAudio context has value PA_CONTEXT_READY this method initializes the PulseAudio stream,
 * sets up the write callback for the stream to play the given 'test-sound.wav' file and connects
 * to the initialized stream. When the connection is established, the audio file is played.
 * \param ctx Current PulseAudio context.
 * \param userdata User data passed during the PulseAudio context initialization.
 */
void pa_state_callback(pa_context* ctx, void* userdata) {
    switch (pa_context_get_state(ctx)) {
        case PA_CONTEXT_UNCONNECTED:
        case PA_CONTEXT_CONNECTING:
        case PA_CONTEXT_AUTHORIZING:
        case PA_CONTEXT_SETTING_NAME:
        default:
            break;
        case PA_CONTEXT_FAILED:
            cout << __func__ << ": PA_CONTEXT_FAILED" << endl;
            free_pulse_audio();
            exit(1);
        case PA_CONTEXT_TERMINATED:
            cout << __func__ << ": PA_CONTEXT_TERMINATED" << endl;
            free_pulse_audio();
            exit(1);
        case PA_CONTEXT_READY:
            pa_sample_spec sample_spec = { PA_SAMPLE_S16NE, 16000, 1 };
            pastream = pa_stream_new(pa_ctx, "playback", &sample_spec, NULL);
            if (!pastream) {
                cout << __func__ << ": pa_stream_new() failed." << endl;
                free_pulse_audio();
                exit(1);
            }
            pa_stream_set_write_callback(pastream, play_audio_stream, fopen("test-sound.wav", "r"));
            if (pa_stream_connect_playback(pastream, NULL, NULL, PA_STREAM_NOFLAGS, NULL, NULL) < 0) {
                cout << __func__ << ": pa_stream_connect_playback() failed." << endl;
                free_pulse_audio();
                exit(1);
            }
            break;
    }
}

/*!
 * \brief Handles the audio resource acquiring state.
 * This method initializes the PulseAudio loop and context when the audio resource is acquired
 * by this app. Also this method run the connection to the initialized PulseAudio context.
 * When the audio resource is released this method stops playing audio and closes the PulseAudio stream.
 * \param audio_resource Current audioresource_t instance.
 * \param acquired True if the audio resource is acquired by this app, false — if released.
 * \param userdata User data passed during the audioresource instance initialization.
 */
void on_acquired(audioresource_t* audio_resource, bool acquired, void* userdata) {
    if (acquired) {
        cout << __func__ << " acquired" << endl;
        pa_glml = pa_glib_mainloop_new(NULL);
        pa_mainloop_api* pa_ml_api = pa_glib_mainloop_get_api(pa_glml);
        pa_ctx = pa_context_new(pa_ml_api, "pulseaudio-example");
        pa_context_set_state_callback(pa_ctx, pa_state_callback, NULL);
        pa_context_connect(pa_ctx, NULL, pa_context_flags_t(0), NULL);
    } else {
        cout << __func__ << " release" << endl;
        stop_audio_stream(pastream, false);
    }
}

/*!
 * \brief Runs the application to play the 'test-sound.wav' audio file.
 * This method initializes the audioresource_t instance and sets up the handler
 * to acquire the audio resource.
 * Then it tries to acquire the audio resource and starts the Glib even loop.
 * When the Glib loop is stopped it frees the PulseAudio stream, context and loop instances,
 * releases the audio resources and frees it.
 * The app plays the audio file twice to make sure that PulseAudio Acynchronous API, libaudioresurce
 * and Glib even loop works correctly together.
 *
 * @see https://git.merproject.org/mer-core/libaudioresource
 */
int main(int argc, char* argv[]) {
    audioresource_t *resource;
    resource = audioresource_init(AUDIO_RESOURCE_MEDIA, on_acquired, NULL);
    for (int i = 0; i < 2; i++) {
        audioresource_acquire(resource);
        glib_loop_stopped = false;
        while (!glib_loop_stopped) {
            g_main_context_iteration(NULL, false);
        }
        free_pulse_audio();
        audioresource_release(resource);
    }
    audioresource_free(resource);
    return 0;
}
