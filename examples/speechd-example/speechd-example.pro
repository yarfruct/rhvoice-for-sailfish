TARGET = speechd-example

CONFIG += sailfishapp

INCLUDEPATH += /usr/include/speech-dispatcher

LIBS += -lspeechd

SOURCES += src/speechd-example.cpp \
    src/speechdispatchermediator.cpp

DISTFILES += qml/speechd-example.qml \
    qml/cover/CoverPage.qml \
    qml/pages/FirstPage.qml \
    rpm/speechd-example.spec \
    rpm/speechd-example.yaml \
    translations/*.ts \
    speechd-example.desktop \
    qml/pages/SelectVoicePage.qml

SAILFISHAPP_ICONS = 86x86 108x108 128x128 172x172

CONFIG += sailfishapp_i18n

TRANSLATIONS += translations/speechd-example.ts

HEADERS += \
    src/speechdispatchermediator.h
