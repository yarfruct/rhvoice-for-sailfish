import QtQuick 2.0
import Sailfish.Silica 1.0
import org.fruct 1.0

Page {

    SpeechDispatcherMediator {
        id: speechDispatcherMediator
    }

    SilicaFlickable {
        anchors.fill: parent

        Column {
            id: column
            width: parent.width
            spacing: Theme.paddingMedium

            PageHeader {
                title: qsTr("Speech Dispatcher Example")
            }

            TextField {
                id: textToSpeechField
                width: parent.width
                placeholderText: qsTr("Enter text here")
                label: qsTr("Text to speech")
                text: "Hello darkness, my old friend. I've come to talk with you again"
            }
            Row {
                anchors.horizontalCenter: parent.horizontalCenter
                spacing: Theme.paddingMedium
                Button {
                    id: sayButton
                    text: qsTr("Say")
                    onClicked: {
                        // starts the entered text speaking
                        speechDispatcherMediator.say(textToSpeechField.text);
                    }
                }
                Button {
                    id: stopButton
                    text: qsTr("Stop")
                    onClicked: {
                        // stops the text speaking
                        speechDispatcherMediator.stop();
                    }
                }
            }
            ValueButton {
                id: voiceValueButton
                width: parent.width
                label: qsTr("Voice")
                value: speechDispatcherMediator.voice.name === undefined
                       ? qsTr("Select")
                       : speechDispatcherMediator.voice.name + " - " + speechDispatcherMediator.voice.language
                onClicked: {
                    // opens a page to select a voice from the list of available voices and saves
                    // the voice using SpeechDispatcherMediator when the list view item is chosen
                    var page = pageStack.push(Qt.resolvedUrl("SelectVoicePage.qml"),
                                              {voices: speechDispatcherMediator.availableVoices()});
                    page.voiceChosen.connect(function(voice) { speechDispatcherMediator.voice = voice });
                }
            }
            Slider {
                id: volumeSlider
                width: parent.width
                minimumValue: -100
                maximumValue: 100
                stepSize: 10
                value: 0
                valueText: value
                label: qsTr("Volume")
                onSliderValueChanged: {
                    // sets up the text-to-speech volume
                    speechDispatcherMediator.setVolume(value);
                }
            }
            Slider {
                id: rateSlider
                width: parent.width
                minimumValue: -100
                maximumValue: 100
                stepSize: 10
                value: 0
                valueText: value
                label: qsTr("Rate")
                onSliderValueChanged: {
                    // sets up the text-to-speech rate
                    speechDispatcherMediator.setRate(value);
                }
            }
            Slider {
                id: pitchSlider
                width: parent.width
                minimumValue: -100
                maximumValue: 100
                stepSize: 10
                value: 0
                valueText: value
                label: qsTr("Pitch")
                onSliderValueChanged: {
                    // sets up the text-to-speech pitch
                    speechDispatcherMediator.setPitch(value);
                }
            }
        }
    }
}
