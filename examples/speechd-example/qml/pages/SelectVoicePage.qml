import QtQuick 2.0
import Sailfish.Silica 1.0

Page {

    // list of available voices to choose
    property var voices

    // emits when the voice is chosen in the list view on the page
    signal voiceChosen(var voice)

    InteractionHintLabel {
        anchors.centerIn: parent
        text: qsTr("There is no available voices")
        backgroundColor: "transparent"
        visible: voicesListView.count === 0 // message about no available voices is displayed only
                                            // when there are no items in the list view
    }

    SilicaListView {
        id: voicesListView
        anchors.fill: parent
        header: PageHeader {
            title: qsTr("Select voice")
        }
        model: voices
        delegate: ListItem {
            anchors { left: parent.left; right: parent.right }
            height: Theme.itemSizeMedium

            Label {
                anchors {
                    left: parent.left; right: parent.right
                    leftMargin: Theme.horizontalPageMargin
                    rightMargin: Theme.horizontalPageMargin
                    verticalCenter: parent.verticalCenter
                }
                text: modelData.name + " - " + modelData.language
            }
            onClicked: {
                voiceChosen(modelData);
                pageStack.pop();
            }
        }
    }
}
