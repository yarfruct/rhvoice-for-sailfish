#ifndef SPEECHDISPATCHERMEDIATOR_H
#define SPEECHDISPATCHERMEDIATOR_H

#include <QObject>
#include <libspeechd.h>
#include <QVariantList>

/*!
 * \brief The SpeechDispatcherMediator class is a mediator between Speech Dispatcher API
 * and QML application code.
 * This class provides an access to libspeechd library API and allows to call methods
 * to say and stop saying a text, to set up the volume, rate and pitch values,
 * to retrieve available voices and to set up the chosen voice.
 */
class SpeechDispatcherMediator : public QObject {
    Q_OBJECT
    Q_PROPERTY(QVariantMap voice READ voice WRITE setVoice NOTIFY voiceChanged)

public:
    explicit SpeechDispatcherMediator(QObject* parent = nullptr);
    ~SpeechDispatcherMediator();
    // methods to use the libspeechd API
    Q_INVOKABLE void say(const QString& text);
    Q_INVOKABLE void stop();
    Q_INVOKABLE void setVolume(const int volume);
    Q_INVOKABLE void setRate(int rate);
    Q_INVOKABLE void setPitch(int pitch);

    Q_INVOKABLE QVariantList availableVoices();

    // getter for property
    QVariantMap voice();

    // setter for property
    void setVoice(QVariantMap newVoice);

private:
    SPDConnection* spdConnection;
    QVariantList voices;

    QVariantMap spdVoice;

    void initAvailableVoicesList();

signals:
    void voiceChanged();
};

#endif // SPEECHDISPATCHERMEDIATOR_H
