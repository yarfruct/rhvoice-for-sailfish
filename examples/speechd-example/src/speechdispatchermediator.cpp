#include "speechdispatchermediator.h"
#include <QDebug>

/*!
 * \brief Constructor initializes the SPDConnection object and list of available output modules.
 * Also the constructor sets the current module as 'rhvoice' and sets the default values of
 * the volume, rate and pitch.
 * \param parent The parent QObject instance.
 */
SpeechDispatcherMediator::SpeechDispatcherMediator(QObject* parent) : QObject(parent) {
    spdConnection = spd_open("speechd-example", NULL, NULL, SPD_MODE_SINGLE);
    int result = spd_set_output_module(spdConnection, "rhvoice");
    qDebug() << "spd_set_output_module result: " << result;
    initAvailableVoicesList();
    QVariantMap initVoice{{"name", "Alan"}, {"language", "en"}};
    setVoice(initVoice);
    setVolume(0);
    setRate(0);
    setPitch(0);
}

/*!
 * \brief Destructor closes the current SPD connection.
 */
SpeechDispatcherMediator::~SpeechDispatcherMediator() {
    spd_close(spdConnection);
}

/*!
 * \brief Runs the spd_say() function for reading the given text via speech engine.
 * \param text The text to speech.
 */
void SpeechDispatcherMediator::say(const QString& text) {
    int result = spd_say(spdConnection, SPD_TEXT, text.toUtf8().constData());
    qDebug() << "spd_say result: " << result;
}

/*!
 * \brief Runs the spd_stop() function to stop the text that is being read.
 */
void SpeechDispatcherMediator::stop() {
    int result = spd_stop(spdConnection);
    qDebug() << "spd_say stop: " << result;
}

/*!
 * \brief Runs the spd_set_volume() function to set the volume for reading text.
 * \param volume The volume to set (from -100 to 100).
 */
void SpeechDispatcherMediator::setVolume(const int volume) {
    int result = spd_set_volume(spdConnection, volume);
    qDebug() << "spd_set_volume result: " << result;
}

/*!
 * \brief Runs the spd_set_voice_rate() function to set the rate value for reading text.
 * \param rate The rate value to set (from -100 to 100).
 */
void SpeechDispatcherMediator::setRate(int rate) {
    if (rate == 0) rate = 1;
    int result = spd_set_voice_rate(spdConnection, rate);
    qDebug() << "spd_set_voice_rate result: " << result;
}

/*!
 * \brief Runs the spd_set_voice_pitch() function to set the pitch value for reading text.
 * \param pitch The pitch value to set (from -100 to 100).
 */
void SpeechDispatcherMediator::setPitch(int pitch) {
    if (pitch == 0) pitch = 1;
    int result = spd_set_voice_pitch(spdConnection, pitch);
    qDebug() << "spd_set_voice_pitch result: " << result;
}

/*!
 * \return The list of current available voices' names.
 */
QVariantList SpeechDispatcherMediator::availableVoices() {
    return voices;
}

/*!
 * \return The current chosen voice as QVariantMap.
 */
QVariantMap SpeechDispatcherMediator::voice() {
    return spdVoice;
}

/*!
 * \brief Sets the new voice for the SPDConnection object.
 * This method finds for a voice by a name from the list of available voices,
 * saves the voice as current, sets the voice for the current SPD connection
 * and emits the voiceChanged() signal.
 * \param newVoice The object with the voice data to set.
 */
void SpeechDispatcherMediator::setVoice(QVariantMap newVoice) {
    SPDVoice** spdVoices = spd_list_synthesis_voices(spdConnection);
    for (int i = 0; spdVoices != NULL && spdVoices[i] != NULL; i++) {
        if (strcmp(newVoice["name"].toString().toLocal8Bit().data(), spdVoices[i]->name) == 0) {
            spdVoice = newVoice;
            int result = spd_set_synthesis_voice(spdConnection, spdVoices[i]->name);
            qDebug() << "spd_set_synthesis_voice result: " << result;
            emit voiceChanged();
            return;
        }
    }
}

/*!
 * \brief Initializes the list of the current available voices.
 */
void SpeechDispatcherMediator::initAvailableVoicesList() {
    SPDVoice** spdVoices = spd_list_synthesis_voices(spdConnection);
    for (int i = 0; spdVoices != NULL && spdVoices[i] != NULL; i++) {
        QVariantMap voiceMap{{"name", spdVoices[i]->name}, {"language", spdVoices[i]->language}};
        voices.append(voiceMap);
    }
}
