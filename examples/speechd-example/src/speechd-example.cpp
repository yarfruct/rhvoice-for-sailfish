#include <QtQuick>

#include <sailfishapp.h>
#include "speechdispatchermediator.h"

int main(int argc, char *argv[]) {
    qmlRegisterType<SpeechDispatcherMediator>("org.fruct", 1, 0, "SpeechDispatcherMediator");
    return SailfishApp::main(argc, argv);
}
