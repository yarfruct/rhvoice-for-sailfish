import QtQuick 2.0
import Sailfish.Silica 1.0

Page {

    // list of available voices or locales in depend on the 'areVoices' property:
    // if 'areVoices' is true 'items' is a list of voices, otherwise — list of locales
    property var items

    // if true than this page is a page to select a voice, otherwise — to select a locale
    property bool areVoices: true

    // emits when the item (voice or locale) is chosen in the list view on the page
    signal itemChosen(var item)

    InteractionHintLabel {
        anchors.centerIn: parent
        text: qsTr("There is no available").concat(" ")
                    .concat(areVoices ? qsTr("voices") : qsTr("locales"))
        backgroundColor: "transparent"
        visible: itemsListView.count === 0 // message about no available items (voices or locales)
                                           // is displayed only when there are no such items
    }

    SilicaListView {
        id: itemsListView
        anchors.fill: parent
        header: PageHeader {
            title: qsTr("Select").concat(" ").concat(areVoices ? qsTr("voice") : qsTr("locale"))
        }
        model: items
        delegate: ListItem {
            anchors { left: parent.left; right: parent.right }
            height: Theme.itemSizeMedium

            Label {
                anchors {
                    left: parent.left; right: parent.right
                    leftMargin: Theme.horizontalPageMargin
                    rightMargin: Theme.horizontalPageMargin
                    verticalCenter: parent.verticalCenter
                }
                text: modelData
            }
            onClicked: {
                itemChosen(modelData);
                pageStack.pop();
            }
        }
    }
}
