import QtQuick 2.0
import Sailfish.Silica 1.0
import org.fruct 1.0

Page {

    QtSpeechMediator {
        id: qtSpeechMediator
    }

    states: [
        State { // state of UI elements when the text-to-speech has a state "Ready"
            when: qtSpeechMediator.state === 0
            PropertyChanges { target: textToSpeechField; enabled: true } // enable the input field
            PropertyChanges { target: errorLabel; visible: false } // do not display a label with error message
            PropertyChanges { target: sayButton; enabled: true } // enable a button to start saying the entered text
            PropertyChanges { target: stopButton; enabled: false } // disable a button to stop saying the text
            PropertyChanges { target: voiceValueButton; enabled: true } // enable a button to open a page to select a voice
            PropertyChanges { target: localeValueButton; enabled: true } // enable a button to open a page to select a locale
            PropertyChanges { target: volumeSlider; enabled: true } // enable a button to set up the volume
            PropertyChanges { target: rateSlider; enabled: true } // enable a button to set up the rate
            PropertyChanges { target: pitchSlider; enabled: true } // enable a button to set up the pitch
        },
        State { // state of UI elements when the text-to-speech has a state "Speaking"
            when: qtSpeechMediator.state === 1
            PropertyChanges { target: textToSpeechField; enabled: false } // disable the input field
            PropertyChanges { target: errorLabel; visible: false } // do not display a label with error message
            PropertyChanges { target: sayButton; enabled: false } // disable a button to start saying the entered text
            PropertyChanges { target: stopButton; enabled: true } // enable a button to stop saying the text
            PropertyChanges { target: voiceValueButton; enabled: false } // disable a button to open a page to select a voice
            PropertyChanges { target: localeValueButton; enabled: false } // disable a button to open a page to select a locale
            PropertyChanges { target: volumeSlider; enabled: false } // disable a button to set up the volume
            PropertyChanges { target: rateSlider; enabled: false } // disable a button to set up the rate
            PropertyChanges { target: pitchSlider; enabled: false } // disable a button to set up the pitch
        },
        State { // state of UI elements when the text-to-speech has a state "BackendError"
            when: qtSpeechMediator.state === 3
            PropertyChanges { target: textToSpeechField; enabled: true } // enable the input field
            PropertyChanges { target: errorLabel; visible: true } // display a label with error message
            PropertyChanges { target: sayButton; enabled: true } // enable a button to start saying the entered text
            PropertyChanges { target: stopButton; enabled: false } // disable a button to stop saying the text
            PropertyChanges { target: voiceValueButton; enabled: true } // enable a button to open a page to select a voice
            PropertyChanges { target: localeValueButton; enabled: true } // enable a button to open a page to select a locale
            PropertyChanges { target: volumeSlider; enabled: true } // enable a button to set up the volume
            PropertyChanges { target: rateSlider; enabled: true } // enable a button to set up the rate
            PropertyChanges { target: pitchSlider; enabled: true } // enable a button to set up the pitch
        }
    ]

    SilicaFlickable {
        anchors.fill: parent
        contentHeight: column.height

        Column {
            id: column
            width: parent.width
            spacing: Theme.paddingMedium

            PageHeader {
                title: qsTr("QtSpeech Example")
            }

            TextField {
                id: textToSpeechField
                width: parent.width
                placeholderText: qsTr("Enter text here")
                label: qsTr("Text to speech")
                text: "Hello darkness, my old friend. I've come to talk with you again"
            }
            Label {
                id: errorLabel
                width: parent.width
                wrapMode: Text.WordWrap
                text: qsTr("The backend was unable to synthesize the current string")
                color: "red"
            }
            Row {
                anchors.horizontalCenter: parent.horizontalCenter
                spacing: Theme.paddingMedium
                Button {
                    id: sayButton
                    text: qsTr("Say")
                    onClicked: {
                        // starts the entered text speaking
                        qtSpeechMediator.say(textToSpeechField.text);
                    }
                }
                Button {
                    id: stopButton
                    text: qsTr("Stop")
                    onClicked: {
                        // stops the text speaking
                        qtSpeechMediator.stop();
                    }
                }
            }
            ValueButton {
                id: localeValueButton
                width: parent.width
                label: qsTr("Locale")
                value: qtSpeechMediator.locale.length === 0 ? qsTr("Select")
                                                            : qtSpeechMediator.locale
                onClicked: {
                    // opens a page to select a locale from the list of available locales
                    // and saves the locale using QtSpeechMediator when the list view item is chosen
                    var page = pageStack.push(Qt.resolvedUrl("SelectPage.qml"),
                                              {items: qtSpeechMediator.availableLocales(),
                                               areVoices: false});
                    page.itemChosen.connect(function(item) { qtSpeechMediator.locale = item });
                }
            }
            ValueButton {
                id: voiceValueButton
                width: parent.width
                label: qsTr("Voice")
                value: qtSpeechMediator.voice.length === 0 ? qsTr("Select")
                                                           : qtSpeechMediator.voice
                onClicked: {
                    // opens a page to select a voice from the list of available voices
                    // and saves the voice using QtSpeechMediator when the list view item is chosen
                    var page = pageStack.push(Qt.resolvedUrl("SelectPage.qml"),
                                              {items: qtSpeechMediator.availableVoices()});
                    page.itemChosen.connect(function(item) { qtSpeechMediator.voice = item });
                }
            }
            Slider {
                id: volumeSlider
                width: parent.width
                minimumValue: 0
                maximumValue: 100
                stepSize: 10
                value: 70
                valueText: value + "%"
                label: qsTr("Volume")
                onSliderValueChanged: {
                    // sets up the text-to-speech volume
                    qtSpeechMediator.setVolume(value);
                }
            }
            Slider {
                id: rateSlider
                width: parent.width
                minimumValue: -1.0
                maximumValue: 1.0
                stepSize: 0.1
                value: 0.0
                valueText: value
                label: qsTr("Rate")
                onSliderValueChanged: {
                    // sets up the text-to-speech rate
                    qtSpeechMediator.setRate(value);
                }
            }
            Slider {
                id: pitchSlider
                width: parent.width
                minimumValue: -1.0
                maximumValue: 1.0
                stepSize: 0.1
                value: 0.0
                valueText: value
                label: qsTr("Pitch")
                onSliderValueChanged: {
                    // sets up the text-to-speech pitch
                    qtSpeechMediator.setPitch(value);
                }
            }
        }
    }
}
