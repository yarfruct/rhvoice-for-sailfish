TARGET = qtspeech-example

INCLUDEPATH += $$[QT_INSTALL_HEADERS]/QtTextToSpeech
LIBS += -lQt5TextToSpeech

CONFIG += sailfishapp

SOURCES += src/qtspeech-example.cpp \
    src/qtspeechmediator.cpp

DISTFILES += qml/qtspeech-example.qml \
    qml/cover/CoverPage.qml \
    qml/pages/FirstPage.qml \
    rpm/qtspeech-example.spec \
    rpm/qtspeech-example.yaml \
    translations/*.ts \
    qtspeech-example.desktop \
    qml/pages/SelectPage.qml

SAILFISHAPP_ICONS = 86x86 108x108 128x128 172x172

CONFIG += sailfishapp_i18n

TRANSLATIONS += translations/qtspeech-example.ts

HEADERS += \
    src/qtspeechmediator.h
