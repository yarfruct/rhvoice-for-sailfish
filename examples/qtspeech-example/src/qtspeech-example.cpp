#include <QtQuick>
#include <sailfishapp.h>

#include "qtspeechmediator.h"

int main(int argc, char* argv[]) {
    qmlRegisterType<QtSpeechMediator>("org.fruct", 1, 0, "QtSpeechMediator");
    return SailfishApp::main(argc, argv);
}
