#include "qtspeechmediator.h"

#include <QDebug>

/*!
 * \brief Constructor initializes the QTextToSpeech object, current TTS state and list of available
 * voices. Also the constructor connects the QTextToSpeech signal stateChanged() with
 * the stateChanged() slot of the class.
 * \param parent The parent QObject instance.
 */
QtSpeechMediator::QtSpeechMediator(QObject* parent) : QObject(parent) {
    textToSpeech = new QTextToSpeech(this);
    ttsState = textToSpeech->state();
    initAvailableLocales();
    setLocale("en_US");
    connect(textToSpeech, SIGNAL(stateChanged(QTextToSpeech::State)),
            this, SLOT(stateChanged(QTextToSpeech::State)));
}

/*!
 * \brief Runs the QTextToSpeech::say() method for reading the given text via speech engine.
 * \param text The text to say.
 */
void QtSpeechMediator::say(const QString& text) {
    textToSpeech->say(text);
}

/*!
 * \brief Runs the QTextToSpeech::stop() method to stop the text that is being read.
 */
void QtSpeechMediator::stop() {
    textToSpeech->stop();
}

/*!
 * \brief Runs the QTextToSpeech::setVolume() method to set the volume for reading text.
 * \param volume The volume to set (from 0 to 100).
 */
void QtSpeechMediator::setVolume(const int volume) {
    textToSpeech->setVolume(volume / 100.0);
}

/*!
 * \brief Runs the QTextToSpeech::setRate() method to set the rate value for reading text.
 * \param rate The rate value to set (from -1.0 to 1.0).
 */
void QtSpeechMediator::setRate(double rate) {
    if (rate == 0.0) rate = 0.01;
    textToSpeech->setRate(rate);
}

/*!
 * \brief Runs the QTextToSpeech::setPitch() method to set the pitch value for reading text.
 * \param pitch The pitch value to set (from -1.0 to 1.0).
 */
void QtSpeechMediator::setPitch(double pitch) {
    if (pitch == 0.0) pitch = 0.01;
    textToSpeech->setPitch(pitch);
}

/*!
 * \return The list of available voices as list of voices' names.
 */
QStringList QtSpeechMediator::availableVoices() {
    return voices;
}

/*!
 * \brief Returns the list of the current available locales' names.
 * \return The list of locales' names.
 */
QStringList QtSpeechMediator::availableLocales() {
    return locales;
}

/*!
 * \return The current TTS state.
 */
int QtSpeechMediator::state() {
    return ttsState;
}

/*!
 * \return The current TTS voice name.
 */
QString QtSpeechMediator::voice() {
    return ttsVoice;
}

/*!
 * \return The current TTS locale's name.
 */
QString QtSpeechMediator::locale() {
    return ttsLocale;
}

/*!
 * \brief Sets the new voice for the QTextToSpeech object.
 * This method finds for a voice by a name from the list of available voices,
 * saves the voice as current, sets the voice for the QTextToSpeech object
 * and emits the voiceChanged() signal.
 * \param newVoice The object with the voice data to set.
 */
void QtSpeechMediator::setVoice(const QString& newVoice) {
    for (QVoice voice : textToSpeech->availableVoices()) {
        if (voice.name().compare(newVoice) == 0) {
            ttsVoice = newVoice;
            textToSpeech->setVoice(voice);
            emit voiceChanged();
            return;
        }
    }
}

/*!
 * \brief Sets the new locale for the QTextToSpeech object.
 * This method finds for a locale by a name from the list of available locales,
 * saves the new locale name as current, sets the locale for the QTextToSpeech object,
 * refreshes the current list of available voices and emits the localeChanged() signal.
 * \param newLocale The name of the new locale to set.
 */
void QtSpeechMediator::setLocale(const QString& newLocale) {
    for (QLocale locale : textToSpeech->availableLocales()) {
        if (locale.name().compare(newLocale) == 0) {
            ttsLocale = newLocale;
            textToSpeech->setLocale(locale);
            refreshAvailableVoicesList();
            emit localeChanged();
            return;
        }
    }
}

/*!
 * \brief Initializes the list of available locales' names.
 */
void QtSpeechMediator::initAvailableLocales() {
    for (QLocale locale : textToSpeech->availableLocales()) {
        locales.append(locale.name());
    }
    locales.sort();
}

/*!
 * \brief Refreshes a list of available voices.
 * This method clears the list of voices, initializes it again, sorts the list
 * and sets the current voice as first from the list.
 */
void QtSpeechMediator::refreshAvailableVoicesList() {
    voices.clear();
    for (QVoice voice : textToSpeech->availableVoices()) {
        voices.append(voice.name());
    }
    voices.sort();
    setVoice(voices.at(0));
}

/*!
 * \brief Saves the new TTS state as current and logs the new state to the console.
 * \param state The new TTS state.
 */
void QtSpeechMediator::stateChanged(QTextToSpeech::State state) {
    if (state == QTextToSpeech::Speaking) {
        qDebug() << "QTextToSpeech::Speaking";
    } else if (state == QTextToSpeech::Ready) {
        qDebug() << "QTextToSpeech::Ready";
    } else if (state == QTextToSpeech::Paused) {
        qDebug() << "QTextToSpeech::Paused";
    } else {
        qDebug() << "QTextToSpeech::BackendError";
    }
    ttsState = state;
    emit stateChanged();
}
