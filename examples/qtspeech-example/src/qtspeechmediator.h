#ifndef QTSPEECHMEDIATOR_H
#define QTSPEECHMEDIATOR_H

#include <QObject>
#include <QTextToSpeech>

/*!
 * \brief The QtSpeechMediator class is a mediator between Qt Speech API and QML application code.
 * This class provides an access to Qt Speech API and allows to call methods
 * to say and stop saying a text, to set up the volume, rate and pitch values,
 * to retrieve available voices and locales and to set up the chosen voice and locale.
 */
class QtSpeechMediator : public QObject {
    Q_OBJECT
    Q_PROPERTY(int state READ state NOTIFY stateChanged)
    Q_PROPERTY(QString voice READ voice WRITE setVoice NOTIFY voiceChanged)
    Q_PROPERTY(QString locale READ locale WRITE setLocale NOTIFY localeChanged)

public:
    explicit QtSpeechMediator(QObject *parent = nullptr);

    // methods to use Qt Speech API
    Q_INVOKABLE void say(const QString& text);
    Q_INVOKABLE void stop();
    Q_INVOKABLE void setVolume(const int volume);
    Q_INVOKABLE void setRate(double rate);
    Q_INVOKABLE void setPitch(double pitch);

    Q_INVOKABLE QStringList availableVoices();
    Q_INVOKABLE QStringList availableLocales();

    // getters for properties
    int state();
    QString voice();
    QString locale();

    // setters for properties
    void setVoice(const QString& newVoice);
    void setLocale(const QString& newLocale);

private:
    QTextToSpeech* textToSpeech;
    QStringList voices;
    QStringList locales;

    int ttsState;
    QString ttsVoice;
    QString ttsLocale;

    void initAvailableLocales();
    void refreshAvailableVoicesList();

private slots:
    void stateChanged(QTextToSpeech::State state);

signals:
    void stateChanged();
    void voiceChanged();
    void localeChanged();
};

#endif // QTSPEECHMEDIATOR_H
